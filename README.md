# alt34

![alt34](/images/alt34_front.png)

## Design

`alt34` was designed as a minimalistic, 'no frills' workhorse keyboard with focus on efficiency and ergonomics.\
This breaks down into the following priorities in the design:

- Compact 34 key split layout with two thumb keys per hand
- Orthogonal layout with a somewhat aggressive column stagger
- Compatibility with standard controllers (Elite-C, Pro Micro etc.)
- Cherry MX switch support
- Kailh hotswap socket support
- Tenting puck support

## Recommended layout

Recommended layout for `alt34` is [miryoku](https://github.com/manna-harbour/miryoku).

## Bill of materials

All components are shown with quantities for a complete pair.

| #   | Qty | Part                                 | Comment                        |
| --- | --- | ------------------------------------ | ------------------------------ |
| 1   | 2   | PCB                                  |                                |
| 2   | 2   | Bottom plate (acrylic or fr4)        | 3mm acrylic recommended        |
| 3   | 2   | Switch plate                         |                                |
| 4   | 2   | MCU cover                            | Optional                       |
| 5   | 2   | Bottom foam insert                   | Optional                       |
| 6   | 2   | Top foam insert                      | Optional                       |
| 7   | 34  | Diode SOD123                         |                                |
| 8   | 4   | 4.7k resistor 0805                   | Only needed for i2c            |
| 9   | 2   | TRRS jack PJ-320A                    |                                |
| 10  | 1   | TRRS cable                           |                                |
| 11  | 34  | Kailh hotswap sockets                |                                |
| 12  | 4   | Millmax ultra low profile sockets    |                                |
| 13  | 2   | Pro Micro compatible microcontroller |                                |
| 14  | 18  | M2x4mm screw                         | For switch plate and MCU cover |
| 15  | 10  | M2x6mm screw                         | For bottom plate               |
| 16  | 10  | M2x6mm spacer                        | For MCU cover                  |
| 17  | 10  | M2x7mm spacer                        | For bottom/switch plates       |
| 18  | 2   | Splitkb.com tenting puck             | Optional                       |
| 19  | 2   | Tripod for tenting puck              | Optional                       |
| 20  | 34  | Cherry MX style switches             |                                |
| 21  | 34  | Cherry MX style keycaps              |                                |
| 22  | 8   | Rubber bumpons                       |                                |

## How to build it

The steps needed to build `alt34` are;

- Export the gerber files for the PCB, MCU cover and switch plate according to the requirements of your PCB manufacturer.
- Export the bottom plate and optionally the foam inserts from the `alt34.svg` file, just enable the corresponding layers.
- Order the needed parts according to the BOM. (Laser cutting recommended for acrylic bottom plate)
- Assemble the keyboard, look up a guide for `crkbd` if needed.
- Flash it with [QMK](https://github.com/qmk/qmk_firmware), alt34 is found in the [official repo](https://github.com/qmk/qmk_firmware/tree/master/keyboards/alt34).

## Why 'alt34'?

There are several reasons for the name `alt34`:

- `34` is the number of keys on the design (duh)
- `alt` is a common key on a keyboard (double duh), as well as an acronym of letters in my name
- `alt34` is also short for "alternative 34", as in "yet another custom keyboard alternative"

## Revision history

| Rev | Date       | Changes                                                   |
| --- | ---------- | --------------------------------------------------------- |
| 1B  | 2022-05-28 | Move screw hole to avoid minor interference with switches |
| 1A  | 2022-01-09 | Initial implementation                                    |

## Acknowledgements

`alt34` was heavily inspired by [crkbd](https://github.com/foostan/crkbd), [fifi](https://github.com/raychengy/fifi_split_keeb), [Ferris](https://github.com/pierrechevalier83/ferris)/[Sweep](https://github.com/davidphilipbarr/sweep) and [IMK](https://imkulio.com/)

## License

`alt34` is licensed under the terms of [GNU GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html)

## Say thanks by donating!

[![paypal](https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=LFV64G9P4AU9Y&currency_code=EUR&source=url)

Copyright © 2022 Tommy Alatalo
